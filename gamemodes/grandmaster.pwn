/*

	This gamemode has been created by Liberty City Development Team for lc-rp.co.
	All rights reserved by LC-RP.CO.

*/

#include 	<a_samp>

#define 	GM_VER	"LC-RP 0.0.1"

#include 	<a_mysql>
#include 	<crashdetect>
#include 	<foreach>
#include 	<zcmd>
#include 	<sscanf2>
#include 	<easyDialog>
#include 	<kickban>
#include 	<streamer>


// Server
#include 	"..\modules\data\headerServer.inc"

main() {
	print("gamemode created by Liberty City Roleplay Development Team for lc-rp.co.");
	printf("gamemode version: %s", GM_VER);
	return 1;
}

public OnGameModeInit() {
	print("preparing the gamemode, please wait ...");
	MySQL_Init();
	return 1;
}

public OnGameModeExit() {
	print("exiting the gamemode, please wait ...");
	MySQL_Exit();
	return 1;
}